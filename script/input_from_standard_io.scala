import scala.io.StdIn

val name = StdIn.readLine("Please input your name: ")
println("Hello %s!".format(name))

println("Please input your age: ")
val age = StdIn.readInt()
println("%s is %d years old.".format(name, age))

val list: List[Any] = StdIn.readf("{0}/{1}/{2}")
println("year: %s".format(list(0)))
println("month: %s".format(list(1)))
println("day: %s".format(list(2)))

val (year: Any, month: String, day: String) = StdIn.readf3("{0}/{1}/{2}")

println("year: %s".format(year))
println("month: %s".format(month))
println("day: %s".format(day))