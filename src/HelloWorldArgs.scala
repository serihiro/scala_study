/**
 * Created by seri on 3/15/15.
 */
object HelloWorldArgs {
  def main(args: Array[String]): Unit = {
    val name: String = args(0)
    println("Hello %s!".format(name))
  }
}
