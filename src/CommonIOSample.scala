import java.io._
import java.net._
import org.apache.commons.io._

/**
 * Created by seri on 3/15/15.
 */
object CommonIOSample extends App {
  val in: InputStream = new URL("http://www.google.co.jp/").openStream()
  try {
    println(IOUtils.toString(in))
  } finally {
    IOUtils.closeQuietly(in)
  }
}
